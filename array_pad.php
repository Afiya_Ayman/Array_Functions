<?php

$input = array(12, 10, 9);

print_r (array_pad($input, 5, 0));
// result is array(12, 10, 9, 0, 0)

echo "<hr>";

print_r(array_pad($input, -7, -1) );
// result is array(-1, -1, -1, -1, 12, 10, 9)

echo "<hr>";
print_r(array_pad($input, 2, "noop") );
// not padded