<?php

$people = array("Peter", "Joe", "Glenn", "Cleveland");
print_r (each($people));

echo "<hr>";
$foo = array("bob", "fred", "jussi", "jouni", "egon", "marliese");
$bar = each($foo);
print_r($bar);

echo "<hr>";
$fruit = array('a' => 'apple', 'b' => 'banana', 'c' => 'cranberry');

reset($fruit);
while (list($key, $val) = each($fruit)) {
    echo "$key => $val\n";
}