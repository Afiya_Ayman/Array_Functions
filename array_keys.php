<?php

$array = array(0 => 100, "color" => "red");
print_r(array_keys($array));

echo "<hr>";
$array = array("blue", "red", "green", "blue", "blue");
print_r(array_keys($array, "blue"));

echo "<hr>";
$array = array("color" => array("blue", "red", "green"),
    "size"  => array("small", "medium", "large"));
print_r(array_keys($array));