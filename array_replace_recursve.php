<?php

$base = array('citrus' => array( "orange") , 'berries' => array("blackberry", "raspberry"), );
$replacements = array('citrus' => array('pineapple'), 'berries' => array('blueberry'));
print_r($base);
echo "<hr>";

print_r($replacements);
echo "<hr>";

$basket = array_replace_recursive($base, $replacements);
print_r($basket);
echo "<hr>";

$basket = array_replace($base, $replacements);
print_r($basket);